


<?php

$start = microtime(true);

$nb_it = 0;
$comp = 0;

function quicksort($argv){
   
    global $nb_it, $comp;

    if (count($argv) == 0)
        return array();

    $pivot_element = $argv[0];
    $left_element = $right_element = array();

    for ($i = 1; $i < count($argv); $i++) {
        $comp++;
        if ($argv[$i] <$pivot_element){

            $left_element[] = $argv[$i];
         $nb_it++;

        }
            
        else
            $right_element[] = $argv[$i];
    }

    return array_merge(quicksort($left_element), array($pivot_element), quicksort($right_element));
}

foreach ($argv as $arg) {
    $e=explode(";",$arg);
}
echo "Série : " ;
echo implode('; ',$e);
echo "\n"; 
echo "Résultats : " ;
echo implode(",",quicksort($e));
echo "\n"; 
echo "Nb de comparaison : " .$comp ;
echo "\n"; 
 echo "Nb d'itération : " .$nb_it ;
 echo "\n"; 

 $total = microtime(true) - $start;

 echo "Temps (sec) : " .(round($total, 2));

  

?>

