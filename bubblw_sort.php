<?php


$start = microtime(true);

$nb_it = 0;
$comp = 0;

function bubble_sort($argv){

    global $nb_it, $comp;
    $n = count($argv);
    $flag;
    for($i=0;$i<$n;$i++){
        $flag = false;
        $comp++;
        for($j=0;$j<$n-$i-1; $j++){
            if($argv[$j]>$argv[$j+1]){
                $tmp = $argv[$j];
                $argv[$j] = $argv[$j+1];
                $argv[$j+1] = $tmp;
                $nb_it++;
            }
        }
    }
    return $argv;
}

foreach ($argv as $arg) {
    $e=explode(";",$arg);
}
echo "Série : " ;
echo implode('; ',$e);
echo "\n"; 
echo "Résultats : " ;
echo implode(",",bubble_sort($e));
echo "\n"; 

echo "Nb de comparaison : " .$comp ;
echo "\n"; 
echo "Nb d'itération : " .$nb_it ;
echo "\n"; 
$total = microtime(true) - $start;
echo "Temps (sec) : " .(round($total, 2));
echo "\n"; 

?>