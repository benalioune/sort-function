<?php


$start = microtime(true);

$nb_it = 0;
$comp = 0;

function merge_sort($argv)
{
    // si le tableau est de taille 1 on retourne le tableau
    if(count($argv) == 1 ) return $argv;
    
 // sinon on divise le tableau en deux partie 
    $mid = count($argv) / 2; // milieu
    $left = array_slice($argv, 0, $mid); // tableau gauche
    $right = array_slice($argv, $mid); // tableau droite
 
    $left = merge_sort($left);
    $right = merge_sort($right);
     
    return merge($left, $right);
}
 
function merge($left, $right)
{
    $result=array(); // tableau du résultat
    $leftIndex=0; // indice tableau gauche
    $rightIndex=0; // indice tableau droite
    global $nb_it, $comp;
 
    // tant que l'indice gauche plus petit a la taille du tableau gauche et tant aue l'indice droite plsu petit a la taille du tableau droite
    while($leftIndex<count($left) && $rightIndex<count($right))
    {
        $nb_it++;
        // si le premier élément du tableau gauche est supérieur au premier élément du tableau droite
        if($left[$leftIndex]>$right[$rightIndex])
        {
 
            //le on insère le plus petit  qui est le premier élément du tableau droite
            $result[]=$right[$rightIndex];
            // on deplace l'indice du tableau droite
            $rightIndex++;
            $comp++;

           
        }
        else
        {
            $result[]=$left[$leftIndex];
            $leftIndex++;
            $comp++;
        }
    }
    while($leftIndex<count($left))
    {
       
        $result[]=$left[$leftIndex];
        $leftIndex++;
    }
    while($rightIndex<count($right))
    {
        $result[]=$right[$rightIndex];
        $rightIndex++;
    }
    return $result;
}


foreach ($argv as $arg) {
    $e=explode(";",$arg);
}
echo "Série : " ;
echo implode('; ',$e);
echo "\n"; 
echo "Résultats : " ;
echo implode(",",merge_sort($e));
echo "\n"; 

echo "Nb de comparaison : " .$comp ;
echo "\n"; 
echo "Nb d'itération : " .$nb_it ;
echo "\n"; 
$total = microtime(true) - $start;
echo "Temps (sec) : " .(round($total, 2));
echo "\n"; 


?>