<?php


$start = microtime(true);

$nb_it = 0;
$comp = 0;

function shell_sort($arr){
    global $nb_it, $comp;
	$inc = round(count($arr)/2);
	while($inc > 0)
	{
		for($i = $inc; $i < count($arr);$i++){
            $nb_it++;
			$temp = $arr[$i];
			$j = $i;
			while($j >= $inc && $arr[$j-$inc] > $temp){
                $comp++;
				$arr[$j] = $arr[$j - $inc];
				$j -= $inc;
			}
			$arr[$j] = $temp;
		}
		$inc = round($inc/2.2);
	}
	return $arr;
}



foreach ($argv as $arg) {
    $e=explode(";",$arg);
}
echo "Série : " ;
echo implode('; ',$e);
echo "\n"; 
echo "Résultats : " ;
echo implode(",",shell_sort($e));
echo "\n"; 

echo "Nb de comparaison : " .$comp ;
echo "\n"; 
echo "Nb d'itération : " .$nb_it ;
echo "\n"; 
$total = microtime(true) - $start;
echo "Temps (sec) : " .(round($total, 2));
echo "\n"; 


?>