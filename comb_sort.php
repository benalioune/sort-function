<?php


$start = microtime(true);

$nb_it = 0;
$comp = 0;

function comb_sort($arr){
    global $nb_it, $comp;
	$gap = count($arr);
        $swap = true;
	while ($gap > 1 || $swap){
		if($gap > 1) $gap /= 1.25;
 
		$swap = false;
		$i = 0;
		while($i+$gap < count($arr)){
            $nb_it++;
			if($arr[$i] > $arr[$i+$gap]){
                $comp++;
				list($arr[$i], $arr[$i+$gap]) = array($arr[$i+$gap],$arr[$i]);
				$swap = true;
			}
			$i++;
		}
	}
	return $arr;
}


foreach ($argv as $arg) {
    $e=explode(";",$arg);
}
echo "Série : " ;
echo implode('; ',$e);
echo "\n"; 
echo "Résultats : " ;
echo implode(",",comb_sort($e));
echo "\n"; 

echo "Nb de comparaison : " .$comp ;
echo "\n"; 
echo "Nb d'itération : " .$nb_it ;
echo "\n"; 
$total = microtime(true) - $start;
echo "Temps (sec) : " .(round($total, 2));
echo "\n"; 


?>