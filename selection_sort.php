<?php

$start = microtime(true);

$nb_it = 0;
$comp = 0;

function selectionSort($argv){

    global $nb_it, $comp;

        for($i = 0; $i< count($argv);$i++){
        $comp ++;
            $pos = $i;
            for ($j = $i + 1; $j < count($argv); $j++) {
                if ($argv[$j] < $argv[$pos])
                    $pos = $j;
            }

            $tmp = $argv[$i];
            $argv[$i] = $argv[$pos];
            $argv[$pos] = $tmp;
            $nb_it ++;
        }

        return $argv;
    }

    foreach ($argv as $arg) {
        $e=explode(";",$arg);
    }
    echo "Série : " ;
    echo implode('; ',$e);
    echo "\n"; 
    echo "Résultats : " ;
    echo implode(",",selectionSort($e));
    echo "\n"; 
  
echo "Nb de comparaison : " .$comp ;
echo "\n"; 
 echo "Nb d'itération : " .$nb_it ;
 echo "\n"; 
    $total = microtime(true) - $start;
    echo "Temps (sec) : " .(round($total, 2));
    echo "\n"; 
?>